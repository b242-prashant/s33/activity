
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then(json =>{json.map((elem) =>{console.log(elem.title)})})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {console.log(json.title)
if((json.completed)==false){
	console.log("The task is not yet completed");
}
else{
	console.log("The task is completed");	
}
});

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-type':'application/json'
	},
	body:JSON.stringify({
		"userId": 110,
        "title": "New task",
        "completed": true
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body:JSON.stringify({
		"userId": 2,
        "Title": "Updated title",
        "Status": "completed",
        "Date completed": "13th Feb 2023",
        "Description": "This the Updated description"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
		"title": "Update title using PATCH method"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
		"completed": true,
		"Date of completion": "14th of Feb 2023"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',
})
.then((response) => response.json())
.then((json) => console.log(json));